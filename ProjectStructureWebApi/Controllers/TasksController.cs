using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private IService<ProjectTaskDTO> _taskService;
        private QueueServiceBeta _qs;

        public TasksController(IService<ProjectTaskDTO> taskService, QueueServiceBeta qs)
        {
            _taskService = taskService;
            _qs = qs;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ProjectTaskDTO>> Get()
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            return Ok(_taskService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ProjectTaskDTO> Get(int id)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            ProjectTaskDTO foundEntity;
            try
            {
                foundEntity = _taskService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] ProjectTaskDTO value)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            return _taskService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProjectTaskDTO value)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
             try
            {
                if (id == value.Id)
                {
                    _taskService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                _taskService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

    
    }
}