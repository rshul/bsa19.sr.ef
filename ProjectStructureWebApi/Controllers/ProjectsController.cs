
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ProjectStructure.BLL.Hubs;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        
        private IServiceAsync<Project> _projectService;
        private IMapper _mapper; 

        // private QueueServiceBeta _qs;

        public ProjectsController(IServiceAsync<Project> projectService, IMapper mapper, IConfiguration config)
        {
            _projectService = projectService;
            _mapper = mapper;
           // _qs =qs;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            var collection = await _projectService.GetAllAsync();
            var projects = _mapper.Map<ICollection<ProjectDTO>>(collection);
            return Ok(projects);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            Project foundEntity;
            try
            {
                foundEntity = await _projectService.GetAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectDTO value)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var result  = await _projectService.CreateAsync(new Project());
            await _projectService.SaveChangesAsync();
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProjectDTO value)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                if (id == value.Id)
                {
                    await _projectService.UpdateAsync( new Project());
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            await _projectService.SaveChangesAsync();
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                await _projectService.DeleteAsync(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            await _projectService.SaveChangesAsync();
            return NoContent();

        }

    }
}