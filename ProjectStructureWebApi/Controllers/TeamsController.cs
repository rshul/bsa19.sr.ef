using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private IService<TeamDTO> _teamService;
        private QueueServiceBeta _qs;
        public TeamsController(IService<TeamDTO> teamService, QueueServiceBeta qs)
        {
            _teamService = teamService;
            _qs = qs;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            return Ok(_teamService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            TeamDTO foundEntity;
            try
            {
                foundEntity = _teamService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] TeamDTO value)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            return _teamService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TeamDTO value)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                if (id == value.Id)
                {
                    _teamService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                _teamService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }



    }
}