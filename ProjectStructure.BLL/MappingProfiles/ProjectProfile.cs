﻿using AutoMapper;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>()
                .ForMember(e => e.AuthorId, cfg => cfg.MapFrom(dto => dto.Author_Id))
                .ForMember(e => e.TeamId, cfg => cfg.MapFrom(dto => dto.Team_Id))
                .ReverseMap();

        }
    }
}
