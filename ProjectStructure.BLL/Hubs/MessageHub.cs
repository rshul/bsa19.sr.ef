﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Hubs
{
    
    public sealed class MessageHub :Hub
    {
        
        public  Task Send(string message)
        {
            
            return Clients.All.SendAsync("NewMessage", message);
        }
    }
}
