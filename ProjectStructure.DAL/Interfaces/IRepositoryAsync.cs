using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepositoryAsync<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(int id);
        Task<TEntity> InsertAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(int id);
    }
}