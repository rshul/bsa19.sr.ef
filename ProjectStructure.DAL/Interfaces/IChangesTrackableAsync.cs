using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IChangesTrackableAsync
    {
        Task<int> SaveChangesAsync();
    }
}