﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtentions
    {
        public static void Seed(this ModelBuilder mb)
        {
            ICollection<Team> teams = GenerateRandomTeams(5);
            ICollection<User> users = GenerateRandomUsers(teams);
            ICollection<Project> projects = GenerateRandomProjects(users, teams);
            ICollection<ProjectTask> tasks = GenerateRandomTasks(users, projects);
            ICollection<TaskState> taskStates = GenrateTaskStates();

            mb.Entity<Team>().HasData(teams);
            mb.Entity<User>().HasData(users);
            mb.Entity<Project>().HasData(projects);
            mb.Entity<ProjectTask>().HasData(tasks);
            mb.Entity<TaskState>().HasData(taskStates);


        }

        private static ICollection<TaskState> GenrateTaskStates()
        {
            return new List<TaskState>()
            {
                new TaskState()
                {
                    Id = 1,
                    Value = "Created"
                },
                new TaskState()
                {
                    Id = 2,
                    Value = "Started"
                },
                new TaskState()
                {
                    Id = 3,
                    Value = "Finished"
                },
                new TaskState()
                {
                    Id = 4,
                    Value = "Canceled"
                }

            };
        }

        private static ICollection<ProjectTask> GenerateRandomTasks(ICollection<User> users, ICollection<Project> projects)
        {
            //var usersForTasks = users.Where(u => !projects.Select(p => p.AuthorId).Any(ui => u.Id == ui)).ToList();
            var usersForTasks = users.Except(projects.Select(p => p.Author));
            var tasksNum = usersForTasks.Count();
            var enUsers = usersForTasks.GetEnumerator();
            var en = Enumerable.Range(1, tasksNum).GetEnumerator();

            var tasksFake = new Faker<ProjectTask>()
                .RuleFor(o => o.Id, f => { en.MoveNext(); return en.Current; })
                .RuleFor(o => o.PerformerId, f => { enUsers.MoveNext(); return enUsers.Current.Id; })
                .RuleFor(o => o.PojectId, f => enUsers.Current.TeamId)
                .RuleFor(o => o.TaskStateId, f => f.Random.Number(1,4))
                .RuleFor(o => o.Description, f => f.Lorem.Sentences())
                .RuleFor(o => o.Name, f => f.Lorem.Word())
                .RuleFor(o => o.CreatedAt, f => f.Date.Past(f.Random.Number(2, 5), DateTime.Now))
                .RuleFor(o => o.FinishedAt, f => f.Date.Future(f.Random.Number(1, 3), DateTime.Now));

            return tasksFake.Generate(tasksNum);
        }

        private static ICollection<Project> GenerateRandomProjects(ICollection<User> users, ICollection<Team> teams)
        {
            var projNum = teams.Count();
            var en = Enumerable.Range(1, projNum).GetEnumerator();
            var projectsFake = new Faker<Project>()
               .RuleFor(o => o.Id, f => { en.MoveNext(); return en.Current; })
               .RuleFor(o => o.TeamId, f => en.Current)
               .RuleFor(o => o.AuthorId, f => users.Where(u => u.TeamId == en.Current).Select(u => u.Id).FirstOrDefault())
               .RuleFor(o => o.Name, f => f.Lorem.Word())
               .RuleFor(o => o.Description, f => f.Lorem.Sentences())
               .RuleFor(o => o.CreatedAt, f => f.Date.Past(f.Random.Number(2, 5), DateTime.Now))
               .RuleFor(o => o.Deadline, f => f.Date.Future(f.Random.Number(1, 3), DateTime.Now));

            return projectsFake.Generate(projNum);
        }

        private static ICollection<User> GenerateRandomUsers(ICollection<Team> teams)
        {
            var usersNum = teams.Count() * 10;
            var en = Enumerable.Range(1, usersNum).GetEnumerator();
            var UsrsFake = new Faker<User>()
               .RuleFor(o => o.Id, f => { en.MoveNext(); return en.Current; })
               .RuleFor(o => o.TeamId, f => f.PickRandom<Team>(teams).Id)
               .RuleFor(o => o.Email, f => f.Internet.Email())
               .RuleFor(o => o.FirstName, f => f.Name.FirstName())
               .RuleFor(o => o.LastName, f => f.Name.LastName())
               .RuleFor(o => o.Birthday, f => f.Date.Past(f.Random.Number(20, 50), DateTime.Now))
               .RuleFor(o => o.RegisteredAt, f => f.Date.Recent(f.Random.Number(20, 100), DateTime.Now)); ;

            return UsrsFake.Generate(usersNum);
        }

        private static ICollection<Team> GenerateRandomTeams(int v)
        {
            var en = Enumerable.Range(1, v).GetEnumerator();
            var TeamsFake = new Faker<Team>()
               .RuleFor(o => o.Id, f => { en.MoveNext(); return en.Current; })
               .RuleFor(o => o.Name, f => f.Lorem.Word())
               .RuleFor(o => o.CreatedAt, f => f.Date.Recent());

            return TeamsFake.Generate(v);
        }
    }
}
