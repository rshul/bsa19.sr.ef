using System.Collections.Generic;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.Common.Interfaces;

namespace ProjectStructure.DAL.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where  TEntity: class, IEntity
    {
        private readonly List<TEntity> _entityData;
        public GenericRepository(List<TEntity> context)
        {
            this._entityData = context;

        }
        public void Delete(int id)
        {
            _entityData.Remove(GetById(id));
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _entityData;
        }

        public TEntity GetById(int id)
        {
            return _entityData.Find(e => e.Id == id)?? throw new DataEntityAccessException(message: "Not found entity");
        }

        public int Insert(TEntity entity)
        {
           return _entityData.AutoIncAdd(entity);
        }

        public void Update(int id, TEntity entity)
        {
            var foundEntity = GetById(id);
            var index = _entityData.IndexOf(foundEntity);
            _entityData[index] = entity;
            
        }
    }
}