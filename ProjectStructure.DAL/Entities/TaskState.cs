﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Entities
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
