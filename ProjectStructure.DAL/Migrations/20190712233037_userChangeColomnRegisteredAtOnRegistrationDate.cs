﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class userChangeColomnRegisteredAtOnRegistrationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.RenameColumn(
                name: "RegisteredAt",
                table: "Users",
                newName: "RegistrationDate");

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegistrationDate",
                table: "Users",
                newName: "RegisteredAt");

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 80);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 80);

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreationDate", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 7, 12, 19, 25, 8, 996, DateTimeKind.Local).AddTicks(4929), "aut" },
                    { 2, new DateTime(2019, 7, 12, 3, 9, 30, 601, DateTimeKind.Local).AddTicks(4442), "excepturi" },
                    { 3, new DateTime(2019, 7, 12, 13, 16, 31, 328, DateTimeKind.Local).AddTicks(8089), "temporibus" },
                    { 4, new DateTime(2019, 7, 12, 3, 30, 3, 423, DateTimeKind.Local).AddTicks(4202), "laboriosam" },
                    { 5, new DateTime(2019, 7, 12, 8, 24, 37, 366, DateTimeKind.Local).AddTicks(6757), "qui" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 5, new DateTime(2012, 7, 31, 9, 38, 25, 300, DateTimeKind.Local).AddTicks(5283), "Trevor.Conn49@yahoo.com", "Clare", "Luettgen", new DateTime(2019, 6, 19, 8, 8, 9, 57, DateTimeKind.Local).AddTicks(4048), 1 },
                    { 36, new DateTime(2014, 5, 10, 0, 9, 55, 234, DateTimeKind.Local).AddTicks(2060), "Alden.Larkin@yahoo.com", "Dell", "Veum", new DateTime(2019, 4, 28, 13, 57, 4, 634, DateTimeKind.Local).AddTicks(5220), 3 },
                    { 43, new DateTime(1987, 9, 17, 3, 48, 29, 910, DateTimeKind.Local).AddTicks(2303), "Frank.Gerlach@yahoo.com", "Isaiah", "Lynch", new DateTime(2019, 7, 11, 17, 23, 37, 969, DateTimeKind.Local).AddTicks(4163), 3 },
                    { 3, new DateTime(2019, 4, 12, 5, 12, 59, 477, DateTimeKind.Local).AddTicks(5015), "Karlee59@hotmail.com", "Deontae", "Padberg", new DateTime(2019, 6, 6, 4, 5, 18, 542, DateTimeKind.Local).AddTicks(9801), 4 },
                    { 10, new DateTime(2012, 3, 16, 8, 34, 27, 174, DateTimeKind.Local).AddTicks(6594), "Xzavier44@yahoo.com", "Emmitt", "Mann", new DateTime(2019, 6, 15, 9, 12, 29, 492, DateTimeKind.Local).AddTicks(7995), 4 },
                    { 12, new DateTime(1989, 3, 31, 0, 25, 20, 705, DateTimeKind.Local).AddTicks(9021), "Johanna.Zieme28@yahoo.com", "Amira", "Rowe", new DateTime(2019, 7, 2, 7, 47, 22, 743, DateTimeKind.Local).AddTicks(5117), 4 },
                    { 13, new DateTime(2008, 7, 19, 8, 58, 0, 154, DateTimeKind.Local).AddTicks(2782), "Tressie34@yahoo.com", "Scarlett", "Johnston", new DateTime(2019, 7, 1, 23, 54, 16, 385, DateTimeKind.Local).AddTicks(5726), 4 },
                    { 15, new DateTime(1997, 2, 23, 5, 30, 7, 791, DateTimeKind.Local).AddTicks(9900), "Trace_Lindgren@gmail.com", "Connor", "Hand", new DateTime(2019, 6, 5, 22, 7, 44, 117, DateTimeKind.Local).AddTicks(7677), 4 },
                    { 16, new DateTime(1989, 3, 24, 10, 30, 54, 478, DateTimeKind.Local).AddTicks(9345), "Margaretta48@hotmail.com", "Aniya", "Cummerata", new DateTime(2019, 4, 18, 21, 43, 31, 591, DateTimeKind.Local).AddTicks(4175), 4 },
                    { 17, new DateTime(2012, 10, 1, 15, 59, 58, 945, DateTimeKind.Local).AddTicks(4363), "Maryam_Dickens@hotmail.com", "Carmela", "Bauch", new DateTime(2019, 7, 4, 21, 22, 57, 603, DateTimeKind.Local).AddTicks(4418), 4 },
                    { 24, new DateTime(2011, 10, 10, 15, 20, 31, 350, DateTimeKind.Local).AddTicks(5806), "Werner_Gleason18@hotmail.com", "Raegan", "Padberg", new DateTime(2019, 6, 1, 9, 57, 9, 122, DateTimeKind.Local).AddTicks(9213), 4 },
                    { 25, new DateTime(1982, 6, 4, 1, 13, 10, 838, DateTimeKind.Local).AddTicks(859), "Marco9@hotmail.com", "Leora", "Tromp", new DateTime(2019, 7, 5, 1, 9, 6, 834, DateTimeKind.Local).AddTicks(2207), 4 },
                    { 27, new DateTime(2002, 10, 16, 1, 41, 55, 883, DateTimeKind.Local).AddTicks(6376), "Anita46@gmail.com", "Phoebe", "Collins", new DateTime(2019, 5, 9, 13, 46, 55, 351, DateTimeKind.Local).AddTicks(4374), 4 },
                    { 28, new DateTime(2015, 10, 7, 15, 14, 23, 159, DateTimeKind.Local).AddTicks(6444), "Rebeka_Welch@hotmail.com", "Adela", "Schroeder", new DateTime(2019, 6, 13, 8, 12, 47, 186, DateTimeKind.Local).AddTicks(4811), 4 },
                    { 42, new DateTime(1996, 11, 7, 1, 53, 39, 223, DateTimeKind.Local).AddTicks(7433), "Vivianne_Gutkowski@yahoo.com", "Carissa", "Bartoletti", new DateTime(2019, 5, 22, 15, 40, 25, 538, DateTimeKind.Local).AddTicks(5271), 4 },
                    { 48, new DateTime(1980, 10, 10, 18, 33, 16, 720, DateTimeKind.Local).AddTicks(967), "Issac.Watsica@yahoo.com", "Heber", "Cronin", new DateTime(2019, 5, 24, 1, 54, 34, 450, DateTimeKind.Local).AddTicks(5638), 4 },
                    { 18, new DateTime(2006, 9, 29, 16, 8, 40, 997, DateTimeKind.Local).AddTicks(4626), "Kariane91@hotmail.com", "Gwendolyn", "Rau", new DateTime(2019, 6, 24, 3, 33, 41, 666, DateTimeKind.Local).AddTicks(416), 5 },
                    { 19, new DateTime(2017, 7, 16, 11, 41, 39, 829, DateTimeKind.Local).AddTicks(5436), "Elinor_Yost@yahoo.com", "Kendra", "Ankunding", new DateTime(2019, 5, 12, 0, 18, 0, 179, DateTimeKind.Local).AddTicks(6388), 5 },
                    { 21, new DateTime(1979, 9, 12, 1, 48, 6, 830, DateTimeKind.Local).AddTicks(1404), "Albin26@gmail.com", "Rahul", "Blick", new DateTime(2019, 7, 9, 11, 11, 49, 663, DateTimeKind.Local).AddTicks(4379), 5 },
                    { 29, new DateTime(1981, 12, 14, 11, 41, 37, 89, DateTimeKind.Local).AddTicks(9030), "Ephraim32@gmail.com", "Aida", "Mitchell", new DateTime(2019, 6, 6, 1, 11, 58, 853, DateTimeKind.Local).AddTicks(3982), 5 },
                    { 37, new DateTime(2006, 3, 31, 11, 37, 34, 475, DateTimeKind.Local).AddTicks(7920), "Shad.Marks@gmail.com", "Virgie", "O'Kon", new DateTime(2019, 7, 5, 9, 31, 44, 341, DateTimeKind.Local).AddTicks(1238), 5 },
                    { 39, new DateTime(2017, 4, 8, 14, 58, 3, 733, DateTimeKind.Local).AddTicks(1990), "Adriana_Stokes@hotmail.com", "Doug", "Metz", new DateTime(2019, 4, 20, 19, 27, 26, 989, DateTimeKind.Local).AddTicks(4924), 5 },
                    { 35, new DateTime(2006, 7, 5, 7, 20, 56, 753, DateTimeKind.Local).AddTicks(1308), "Alvena74@yahoo.com", "Merle", "Goldner", new DateTime(2019, 6, 28, 2, 17, 39, 396, DateTimeKind.Local).AddTicks(6672), 3 },
                    { 34, new DateTime(2016, 6, 7, 5, 56, 37, 592, DateTimeKind.Local).AddTicks(4195), "Kayleigh_Towne27@yahoo.com", "Hosea", "Homenick", new DateTime(2019, 5, 28, 3, 5, 59, 988, DateTimeKind.Local).AddTicks(782), 3 },
                    { 31, new DateTime(1994, 12, 11, 7, 54, 7, 34, DateTimeKind.Local).AddTicks(2210), "Stefanie5@gmail.com", "Ruben", "Jones", new DateTime(2019, 6, 21, 3, 4, 36, 766, DateTimeKind.Local).AddTicks(6142), 3 },
                    { 22, new DateTime(1986, 6, 22, 20, 25, 33, 159, DateTimeKind.Local).AddTicks(1690), "Kevin_Ferry87@gmail.com", "Samara", "Balistreri", new DateTime(2019, 7, 8, 22, 23, 5, 167, DateTimeKind.Local).AddTicks(2331), 3 },
                    { 8, new DateTime(1988, 12, 30, 10, 50, 36, 684, DateTimeKind.Local).AddTicks(6890), "Lolita14@gmail.com", "Otto", "Mraz", new DateTime(2019, 5, 1, 22, 56, 27, 626, DateTimeKind.Local).AddTicks(6415), 1 },
                    { 9, new DateTime(2017, 7, 31, 14, 5, 20, 223, DateTimeKind.Local).AddTicks(9361), "Laverne.OReilly@gmail.com", "Alexandra", "Jerde", new DateTime(2019, 7, 8, 17, 8, 15, 994, DateTimeKind.Local).AddTicks(7483), 1 },
                    { 14, new DateTime(2001, 4, 9, 13, 23, 34, 136, DateTimeKind.Local).AddTicks(9591), "Helena_Hilll65@gmail.com", "Arnold", "Kerluke", new DateTime(2019, 6, 12, 5, 5, 38, 43, DateTimeKind.Local).AddTicks(6349), 1 },
                    { 20, new DateTime(2002, 6, 20, 20, 20, 2, 625, DateTimeKind.Local).AddTicks(6312), "Ophelia.Armstrong@gmail.com", "Nicholas", "Kerluke", new DateTime(2019, 6, 23, 18, 21, 22, 725, DateTimeKind.Local).AddTicks(3223), 1 },
                    { 30, new DateTime(1998, 10, 5, 18, 0, 28, 575, DateTimeKind.Local).AddTicks(4070), "Kristoffer78@gmail.com", "Blake", "Thompson", new DateTime(2019, 7, 3, 23, 43, 4, 176, DateTimeKind.Local).AddTicks(4963), 1 },
                    { 32, new DateTime(1984, 11, 29, 18, 6, 6, 563, DateTimeKind.Local).AddTicks(8362), "Andy33@yahoo.com", "Myrtie", "Zemlak", new DateTime(2019, 6, 1, 5, 43, 30, 638, DateTimeKind.Local).AddTicks(7234), 1 },
                    { 38, new DateTime(2008, 1, 1, 12, 22, 27, 760, DateTimeKind.Local).AddTicks(619), "William_Considine97@gmail.com", "Waino", "Glover", new DateTime(2019, 6, 29, 2, 21, 25, 699, DateTimeKind.Local).AddTicks(7648), 1 },
                    { 40, new DateTime(1996, 11, 5, 19, 19, 25, 697, DateTimeKind.Local).AddTicks(2160), "Georgiana.Keeling@hotmail.com", "Angeline", "Schaden", new DateTime(2019, 6, 30, 5, 52, 2, 513, DateTimeKind.Local).AddTicks(826), 1 },
                    { 41, new DateTime(2003, 5, 28, 3, 31, 14, 373, DateTimeKind.Local).AddTicks(3284), "Karl_Farrell30@hotmail.com", "Nina", "Keeling", new DateTime(2019, 6, 7, 21, 9, 59, 259, DateTimeKind.Local).AddTicks(1334), 1 },
                    { 44, new DateTime(2007, 7, 31, 11, 24, 59, 905, DateTimeKind.Local).AddTicks(2878), "Nils43@yahoo.com", "Ressie", "Fisher", new DateTime(2019, 5, 19, 20, 43, 28, 629, DateTimeKind.Local).AddTicks(1240), 1 },
                    { 46, new DateTime(1974, 11, 21, 11, 49, 32, 517, DateTimeKind.Local).AddTicks(9043), "Odie65@yahoo.com", "Darryl", "Batz", new DateTime(2019, 6, 6, 16, 9, 17, 452, DateTimeKind.Local).AddTicks(1684), 5 },
                    { 45, new DateTime(1993, 2, 8, 18, 57, 23, 557, DateTimeKind.Local).AddTicks(8893), "Giovanna26@yahoo.com", "Renee", "Cormier", new DateTime(2019, 5, 25, 13, 45, 8, 599, DateTimeKind.Local).AddTicks(9161), 1 },
                    { 50, new DateTime(2012, 5, 17, 15, 56, 5, 905, DateTimeKind.Local).AddTicks(8313), "Clifton20@gmail.com", "Estrella", "Medhurst", new DateTime(2019, 6, 12, 10, 49, 18, 10, DateTimeKind.Local).AddTicks(9000), 1 },
                    { 1, new DateTime(1986, 10, 9, 0, 1, 52, 175, DateTimeKind.Local).AddTicks(7432), "Rosalia.Rau5@hotmail.com", "Geoffrey", "Braun", new DateTime(2019, 6, 22, 21, 50, 4, 798, DateTimeKind.Local).AddTicks(2242), 2 },
                    { 4, new DateTime(2009, 2, 2, 8, 18, 53, 165, DateTimeKind.Local).AddTicks(7697), "Milo89@hotmail.com", "Verdie", "Reichert", new DateTime(2019, 6, 8, 9, 45, 48, 199, DateTimeKind.Local).AddTicks(39), 2 },
                    { 11, new DateTime(2018, 12, 29, 3, 10, 35, 288, DateTimeKind.Local).AddTicks(4781), "Vance94@gmail.com", "Maurice", "Kessler", new DateTime(2019, 6, 9, 6, 44, 19, 503, DateTimeKind.Local).AddTicks(9822), 2 },
                    { 23, new DateTime(2018, 8, 15, 20, 51, 16, 975, DateTimeKind.Local).AddTicks(4133), "Mitchel.Steuber@yahoo.com", "Hillary", "Rohan", new DateTime(2019, 6, 20, 1, 34, 30, 308, DateTimeKind.Local).AddTicks(9998), 2 },
                    { 26, new DateTime(2017, 7, 14, 14, 59, 42, 292, DateTimeKind.Local).AddTicks(1268), "Favian.Satterfield@gmail.com", "Brandyn", "Keebler", new DateTime(2019, 4, 23, 15, 27, 7, 717, DateTimeKind.Local).AddTicks(5640), 2 },
                    { 33, new DateTime(2001, 9, 13, 9, 39, 35, 694, DateTimeKind.Local).AddTicks(5350), "Maynard67@hotmail.com", "Aryanna", "Cummerata", new DateTime(2019, 5, 8, 20, 7, 17, 131, DateTimeKind.Local).AddTicks(5154), 2 },
                    { 2, new DateTime(1996, 4, 17, 14, 42, 35, 400, DateTimeKind.Local).AddTicks(556), "Mollie_Crooks@yahoo.com", "Taryn", "Konopelski", new DateTime(2019, 6, 30, 16, 33, 50, 392, DateTimeKind.Local).AddTicks(1821), 3 },
                    { 6, new DateTime(2000, 7, 16, 19, 48, 37, 426, DateTimeKind.Local).AddTicks(2387), "Samson_Quitzon@gmail.com", "Keaton", "Daniel", new DateTime(2019, 7, 8, 3, 30, 47, 914, DateTimeKind.Local).AddTicks(3455), 3 },
                    { 7, new DateTime(1997, 8, 3, 0, 2, 43, 741, DateTimeKind.Local).AddTicks(2403), "Reggie.Metz@gmail.com", "Ceasar", "Tromp", new DateTime(2019, 5, 27, 4, 22, 9, 309, DateTimeKind.Local).AddTicks(6071), 3 },
                    { 47, new DateTime(2012, 8, 2, 18, 48, 41, 956, DateTimeKind.Local).AddTicks(558), "Rosalee_Spinka43@yahoo.com", "Angie", "Schroeder", new DateTime(2019, 6, 10, 19, 9, 40, 820, DateTimeKind.Local).AddTicks(3222), 1 },
                    { 49, new DateTime(1988, 7, 8, 0, 48, 3, 272, DateTimeKind.Local).AddTicks(351), "Jodie_Flatley14@gmail.com", "Darlene", "Kshlerin", new DateTime(2019, 5, 21, 2, 34, 39, 480, DateTimeKind.Local).AddTicks(5167), 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 5, new DateTime(2019, 4, 22, 18, 40, 20, 957, DateTimeKind.Local).AddTicks(6675), new DateTime(2021, 11, 6, 17, 15, 30, 309, DateTimeKind.Local).AddTicks(5465), @"Fugiat placeat quia natus sequi quia qui blanditiis.
                Qui sit perferendis voluptatem rerum totam quasi distinctio doloremque tenetur.
                Ipsum modi est quo dolorum.", "odit", 1 },
                    { 2, 1, new DateTime(2018, 2, 2, 6, 44, 38, 566, DateTimeKind.Local).AddTicks(480), new DateTime(2021, 12, 5, 22, 0, 33, 558, DateTimeKind.Local).AddTicks(7655), @"Nisi dolores in qui iste velit ut aut.
                Tempore enim error.
                Adipisci enim est.
                Quis qui possimus voluptatem ipsa voluptatum est.", "voluptatem", 2 },
                    { 3, 2, new DateTime(2017, 12, 12, 10, 47, 28, 331, DateTimeKind.Local).AddTicks(1312), new DateTime(2020, 5, 3, 9, 21, 13, 359, DateTimeKind.Local).AddTicks(6021), @"Distinctio commodi sit libero non perspiciatis id excepturi.
                Omnis ratione quia et hic quia.
                Nihil ipsa molestiae aliquam atque soluta cumque sed corporis.", "aut", 3 },
                    { 4, 3, new DateTime(2018, 3, 13, 15, 2, 44, 847, DateTimeKind.Local).AddTicks(3015), new DateTime(2020, 7, 18, 15, 5, 34, 568, DateTimeKind.Local).AddTicks(8127), @"Optio doloribus est sed impedit cupiditate repellendus aut.
                Omnis quia asperiores alias porro reiciendis expedita assumenda quos.
                Possimus cum architecto velit voluptatibus nam autem.
                Excepturi facere maiores et quia.", "aliquam", 4 },
                    { 5, 18, new DateTime(2017, 7, 24, 17, 1, 6, 311, DateTimeKind.Local).AddTicks(2811), new DateTime(2021, 10, 2, 18, 35, 3, 303, DateTimeKind.Local).AddTicks(1602), @"Ut aliquam quia.
                Impedit natus nihil provident qui eaque.", "nisi", 5 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "PojectId", "TaskStateId" },
                values: new object[,]
                {
                    { 5, new DateTime(2019, 2, 12, 20, 28, 8, 393, DateTimeKind.Local).AddTicks(3206), @"Qui nostrum numquam qui et.
                Accusamus aspernatur culpa eos rerum beatae.
                Ducimus sunt et explicabo aliquam voluptatem ad veniam.
                Cumque quia vitae voluptatem molestiae.", new DateTime(2020, 5, 20, 2, 35, 20, 625, DateTimeKind.Local).AddTicks(2667), "ex", 5, 1, 3 },
                    { 36, new DateTime(2017, 2, 23, 9, 39, 37, 32, DateTimeKind.Local).AddTicks(461), @"Nesciunt suscipit atque.
                Nihil cum omnis.
                Provident quisquam sed tempore cumque voluptas natus qui.
                Ea nobis ut molestias.
                Autem omnis vel sit possimus quibusdam occaecati quia dignissimos dolore.", new DateTime(2020, 4, 29, 22, 22, 57, 550, DateTimeKind.Local).AddTicks(3316), "nihil", 36, 3, 3 },
                    { 43, new DateTime(2018, 1, 25, 18, 3, 37, 650, DateTimeKind.Local).AddTicks(9324), @"Voluptas sed ullam.
                Aliquid est sint ducimus odit.
                Quas voluptatem impedit corporis id tempora est iure reiciendis.
                Officia ab nisi aliquid modi.
                Dolorum voluptatem dolore earum repellat.", new DateTime(2020, 7, 12, 11, 21, 44, 809, DateTimeKind.Local).AddTicks(1110), "aut", 43, 3, 1 },
                    { 3, new DateTime(2016, 1, 1, 21, 15, 10, 815, DateTimeKind.Local).AddTicks(4004), @"Mollitia ullam incidunt accusamus cupiditate eum quae ipsam dolor.
                Quis veniam voluptates et dolor sunt suscipit vel quod accusantium.", new DateTime(2020, 4, 6, 12, 45, 51, 734, DateTimeKind.Local).AddTicks(9539), "esse", 3, 4, 1 },
                    { 10, new DateTime(2017, 9, 25, 21, 56, 51, 839, DateTimeKind.Local).AddTicks(3979), @"Deserunt fugit laborum quibusdam at.
                In cum eos quaerat vel nihil sapiente nulla quaerat fugiat.
                Molestiae aut aliquid possimus quaerat minus.
                Dolorem quia qui in iste ea enim eaque sed ab.
                Sed odio rerum quisquam et officiis magni quisquam.
                Animi odio in.", new DateTime(2020, 1, 8, 0, 44, 48, 131, DateTimeKind.Local).AddTicks(8431), "quas", 10, 4, 4 },
                    { 12, new DateTime(2018, 6, 9, 8, 0, 23, 762, DateTimeKind.Local).AddTicks(681), @"Impedit dolores esse qui quo.
                Doloribus aut optio perferendis maiores.", new DateTime(2020, 9, 27, 4, 42, 28, 970, DateTimeKind.Local).AddTicks(5895), "quia", 12, 4, 4 },
                    { 13, new DateTime(2016, 6, 19, 11, 0, 21, 435, DateTimeKind.Local).AddTicks(1974), @"Sed sunt sit.
                Autem porro dignissimos ut amet molestias numquam neque molestiae debitis.
                Excepturi esse nobis labore soluta dolore porro.", new DateTime(2020, 8, 13, 5, 32, 4, 293, DateTimeKind.Local).AddTicks(9493), "nihil", 13, 4, 2 },
                    { 15, new DateTime(2017, 10, 2, 23, 15, 47, 167, DateTimeKind.Local).AddTicks(3800), @"Sapiente harum velit sed ut.
                Qui pariatur veritatis dolores esse.
                Sint labore ullam voluptas dolores ullam sequi aut dolorum eum.", new DateTime(2021, 11, 20, 12, 56, 45, 421, DateTimeKind.Local).AddTicks(5287), "dignissimos", 15, 4, 4 },
                    { 16, new DateTime(2016, 10, 1, 22, 26, 25, 148, DateTimeKind.Local).AddTicks(631), @"Recusandae veniam ut corrupti quas pariatur.
                Vero eveniet ut aut ea expedita.
                Aut hic ut.", new DateTime(2022, 2, 25, 2, 2, 48, 865, DateTimeKind.Local).AddTicks(6167), "ratione", 16, 4, 2 },
                    { 17, new DateTime(2017, 11, 24, 7, 34, 20, 546, DateTimeKind.Local).AddTicks(9916), @"Dolores amet sit enim quidem omnis consequatur id.
                Sed magni et et minus qui quia quidem.
                Sit et eius ipsum odio sed.
                Consequatur quidem sint est quidem inventore blanditiis eum autem quis.", new DateTime(2019, 12, 12, 1, 33, 46, 877, DateTimeKind.Local).AddTicks(2000), "distinctio", 17, 4, 4 },
                    { 24, new DateTime(2016, 5, 14, 8, 50, 42, 73, DateTimeKind.Local).AddTicks(8771), @"Corrupti corrupti quod eius laboriosam voluptas quia quos.
                Voluptatem molestiae eum ut nesciunt numquam est delectus necessitatibus distinctio.
                Accusamus esse aut nihil.", new DateTime(2019, 10, 5, 7, 49, 32, 720, DateTimeKind.Local).AddTicks(919), "facere", 24, 4, 2 },
                    { 25, new DateTime(2018, 10, 6, 0, 45, 20, 597, DateTimeKind.Local).AddTicks(7212), @"Nam voluptatem nobis ex omnis et.
                Est eum laudantium in eius.
                Ab pariatur et corrupti quas quis quo animi neque.", new DateTime(2019, 12, 30, 7, 13, 46, 730, DateTimeKind.Local).AddTicks(210), "est", 25, 4, 4 },
                    { 27, new DateTime(2018, 11, 14, 4, 46, 16, 842, DateTimeKind.Local).AddTicks(5042), @"Unde est ut ut odio eligendi tempore.
                Sed atque suscipit enim sit ut deserunt fuga consequatur tempora.
                Libero vero consequatur ut et.
                Omnis quo voluptas numquam sed culpa qui nobis.
                In eos necessitatibus eveniet.
                Voluptas quaerat molestiae sed ex perspiciatis vel.", new DateTime(2019, 11, 21, 20, 5, 50, 927, DateTimeKind.Local).AddTicks(101), "autem", 27, 4, 2 },
                    { 28, new DateTime(2018, 7, 13, 7, 22, 23, 866, DateTimeKind.Local).AddTicks(5212), @"Rerum dolor autem rem.
                Soluta at ut nam libero et rerum.", new DateTime(2021, 9, 27, 3, 12, 15, 377, DateTimeKind.Local).AddTicks(8161), "officia", 28, 4, 2 },
                    { 42, new DateTime(2015, 6, 21, 7, 43, 58, 467, DateTimeKind.Local).AddTicks(2112), @"Sapiente nam non.
                Deleniti officia accusamus accusamus.
                Laborum quo facilis aut est eligendi dolor minima nostrum.", new DateTime(2020, 3, 12, 5, 42, 54, 279, DateTimeKind.Local).AddTicks(6145), "qui", 42, 4, 2 },
                    { 48, new DateTime(2016, 12, 21, 15, 4, 43, 155, DateTimeKind.Local).AddTicks(3272), @"Laborum iure deserunt possimus nobis voluptas.
                Sequi nemo enim commodi aut eum et non similique.
                Quia error fugit.
                Voluptatem similique non voluptatum.", new DateTime(2019, 10, 22, 18, 31, 35, 500, DateTimeKind.Local).AddTicks(1996), "aspernatur", 48, 4, 4 },
                    { 18, new DateTime(2019, 3, 10, 14, 23, 32, 10, DateTimeKind.Local).AddTicks(6693), @"Et reiciendis accusantium rem velit atque incidunt odit rem facilis.
                Voluptatum sapiente placeat veritatis autem eum repudiandae pariatur.
                Ipsa ratione tempore sed quidem commodi temporibus quasi explicabo eos.
                Voluptate esse velit rerum eos repudiandae laboriosam tempora aperiam.
                Provident qui consequatur quisquam voluptatem voluptate.", new DateTime(2020, 7, 12, 10, 44, 31, 734, DateTimeKind.Local).AddTicks(3623), "repudiandae", 18, 5, 3 },
                    { 19, new DateTime(2016, 4, 25, 21, 14, 17, 528, DateTimeKind.Local).AddTicks(393), @"Expedita nemo quos fugiat ab optio nesciunt perferendis suscipit debitis.
                Inventore voluptas aut molestiae praesentium.
                Est illum blanditiis excepturi natus eos.
                Ipsam a debitis debitis et impedit ut quod provident.
                Voluptatibus fugiat quam quas quia.
                Ullam sint magnam recusandae totam.", new DateTime(2020, 3, 28, 14, 43, 7, 278, DateTimeKind.Local).AddTicks(7898), "voluptatem", 19, 5, 3 },
                    { 21, new DateTime(2018, 5, 25, 6, 21, 0, 921, DateTimeKind.Local).AddTicks(1373), @"Molestiae fuga voluptas.
                Id qui cum quia libero a exercitationem consequatur enim nihil.
                Sint sit laudantium omnis maxime libero officiis eos voluptatem.
                Quam et labore cumque exercitationem repellat molestiae.", new DateTime(2021, 5, 1, 14, 41, 18, 211, DateTimeKind.Local).AddTicks(3247), "sapiente", 21, 5, 1 },
                    { 29, new DateTime(2018, 5, 30, 12, 5, 49, 896, DateTimeKind.Local).AddTicks(1506), @"Voluptate et dolorem aut tenetur nostrum.
                Voluptates adipisci odit quia nostrum voluptas.
                In modi corrupti quam sit earum maiores.
                Omnis sed ducimus distinctio temporibus quaerat molestiae ea autem saepe.
                Expedita quod non perferendis quos sint minima nostrum.", new DateTime(2020, 10, 3, 10, 33, 56, 865, DateTimeKind.Local).AddTicks(186), "rerum", 29, 5, 2 },
                    { 37, new DateTime(2017, 11, 23, 8, 45, 55, 744, DateTimeKind.Local).AddTicks(4330), @"Natus dolores quasi.
                Ut rerum dolores aut aliquid facilis voluptatem ut pariatur quo.
                Magnam perspiciatis nihil porro quis et voluptatibus.
                Quia ad tenetur magnam.
                Ut rem nisi qui.", new DateTime(2020, 5, 15, 0, 19, 36, 956, DateTimeKind.Local).AddTicks(8330), "quisquam", 37, 5, 4 },
                    { 39, new DateTime(2016, 9, 10, 16, 46, 57, 671, DateTimeKind.Local).AddTicks(3001), @"Magnam molestiae recusandae aliquam ea esse animi.
                Id explicabo dolore eaque ut ut sapiente similique sunt.
                Harum incidunt ea nihil magni.
                Vel inventore assumenda aperiam ipsa non voluptatum provident fugiat at.", new DateTime(2019, 12, 4, 13, 37, 19, 268, DateTimeKind.Local).AddTicks(367), "quae", 39, 5, 1 },
                    { 35, new DateTime(2015, 8, 4, 8, 27, 5, 936, DateTimeKind.Local).AddTicks(1408), @"Vel temporibus ut beatae atque quae aperiam fugiat voluptatibus.
                Magnam porro voluptatibus nihil asperiores autem ea nemo quod.
                Quam voluptas eligendi provident ea nihil nemo.
                Fugit omnis odit omnis tenetur nihil veniam.
                Iure cum laboriosam incidunt animi et omnis ipsam velit.", new DateTime(2020, 3, 14, 15, 39, 0, 799, DateTimeKind.Local).AddTicks(3326), "est", 35, 3, 4 },
                    { 34, new DateTime(2018, 2, 22, 0, 24, 24, 627, DateTimeKind.Local).AddTicks(3024), @"Culpa numquam nulla vitae similique dolorem velit est.
                Mollitia suscipit nesciunt occaecati et velit fugit magni sit qui.
                Vero temporibus voluptatibus voluptas animi occaecati autem in libero aspernatur.
                Dolorum dolor explicabo quidem dolorum.
                Qui minima harum laudantium provident explicabo qui commodi saepe.", new DateTime(2022, 7, 3, 2, 11, 29, 111, DateTimeKind.Local).AddTicks(1085), "non", 34, 3, 4 },
                    { 31, new DateTime(2018, 10, 5, 22, 41, 50, 907, DateTimeKind.Local).AddTicks(3322), @"Ab est eaque omnis sed ipsa quo aut id est.
                Neque illo facere perferendis.
                Et sed eum veritatis.
                Dolor dolorem praesentium ea.
                Quia alias necessitatibus quaerat ipsum et dolore esse sunt.
                Odio minus totam.", new DateTime(2020, 9, 24, 15, 24, 18, 805, DateTimeKind.Local).AddTicks(7795), "qui", 31, 3, 1 },
                    { 22, new DateTime(2018, 3, 22, 8, 11, 0, 179, DateTimeKind.Local).AddTicks(8108), @"In iusto perspiciatis aut.
                Non fugiat nihil aut.
                Velit occaecati nemo omnis ut omnis omnis dolor laboriosam.", new DateTime(2020, 4, 10, 14, 7, 57, 466, DateTimeKind.Local).AddTicks(4301), "voluptatem", 22, 3, 2 },
                    { 8, new DateTime(2019, 2, 24, 14, 40, 9, 461, DateTimeKind.Local).AddTicks(4814), @"Inventore cum suscipit facilis debitis.
                Ipsum et dolorem excepturi impedit libero distinctio cupiditate.
                Minus rem quia dolor.", new DateTime(2019, 12, 25, 16, 48, 21, 611, DateTimeKind.Local).AddTicks(4353), "in", 8, 1, 3 },
                    { 9, new DateTime(2017, 10, 22, 13, 6, 54, 800, DateTimeKind.Local).AddTicks(6857), @"Inventore exercitationem doloremque.
                Quibusdam explicabo maiores eum maiores et ab commodi.
                Ipsam ab nobis in laboriosam.
                Aut eos quia velit eos sed.", new DateTime(2021, 7, 20, 1, 43, 28, 126, DateTimeKind.Local).AddTicks(3695), "ut", 9, 1, 4 },
                    { 14, new DateTime(2018, 11, 22, 12, 14, 49, 748, DateTimeKind.Local).AddTicks(4714), @"Perspiciatis qui facere est non quia est error dolorum aut.
                Ea et totam deleniti pariatur.
                Dolore nemo nihil.
                Porro eos cupiditate quo qui.", new DateTime(2019, 9, 14, 2, 15, 24, 375, DateTimeKind.Local).AddTicks(1576), "iure", 14, 1, 2 },
                    { 20, new DateTime(2017, 8, 1, 1, 22, 13, 827, DateTimeKind.Local).AddTicks(579), @"Molestiae quam explicabo dignissimos cum occaecati explicabo illum veniam.
                Fuga est excepturi.
                Et consequuntur sunt eaque maiores excepturi deleniti.
                Asperiores enim non nisi.
                Ipsam architecto laborum.
                Voluptas praesentium saepe aut.", new DateTime(2022, 6, 28, 13, 38, 27, 987, DateTimeKind.Local).AddTicks(3162), "pariatur", 20, 1, 4 },
                    { 30, new DateTime(2016, 11, 7, 15, 11, 2, 391, DateTimeKind.Local).AddTicks(155), @"Minima et voluptatem eos nam deleniti dolores.
                Et et exercitationem et consequuntur atque autem qui.", new DateTime(2020, 8, 18, 18, 27, 50, 202, DateTimeKind.Local).AddTicks(116), "veniam", 30, 1, 3 },
                    { 32, new DateTime(2016, 11, 1, 17, 41, 57, 43, DateTimeKind.Local).AddTicks(5156), @"Provident mollitia quos unde eum dolorem recusandae soluta.
                Deleniti culpa ea quis fugiat.
                Numquam voluptatum iste vitae repudiandae rerum nam repellat id.
                Quasi quibusdam laudantium doloremque sit voluptas consequuntur.
                Sequi cum voluptate qui impedit laboriosam et ipsam.", new DateTime(2020, 7, 8, 3, 54, 48, 233, DateTimeKind.Local).AddTicks(2627), "minus", 32, 1, 2 },
                    { 38, new DateTime(2018, 6, 16, 3, 31, 24, 408, DateTimeKind.Local).AddTicks(4059), @"Fugiat officiis dignissimos officiis illo dolor qui impedit.
                Perspiciatis ut sunt aperiam nemo vitae culpa ad maxime harum.
                Reprehenderit exercitationem voluptates debitis ullam facere.
                Dolorem nemo provident velit distinctio minima reiciendis.", new DateTime(2020, 1, 25, 15, 23, 20, 603, DateTimeKind.Local).AddTicks(8097), "ut", 38, 1, 4 },
                    { 40, new DateTime(2018, 7, 16, 2, 43, 57, 772, DateTimeKind.Local).AddTicks(9425), @"Autem voluptatem esse labore et repellat.
                Ut assumenda delectus.
                Voluptas eos dolorem exercitationem illo odio.
                Facilis error repellendus aut et.", new DateTime(2020, 6, 27, 23, 22, 10, 362, DateTimeKind.Local).AddTicks(9308), "quo", 40, 1, 1 },
                    { 41, new DateTime(2019, 4, 6, 20, 47, 22, 546, DateTimeKind.Local).AddTicks(6268), @"Quas nam molestiae et vel laudantium sunt et assumenda tenetur.
                Neque molestias et.
                Quo eius consectetur saepe id explicabo atque.
                Est maiores cumque.", new DateTime(2020, 9, 25, 12, 32, 31, 11, DateTimeKind.Local).AddTicks(8271), "cumque", 41, 1, 3 },
                    { 44, new DateTime(2017, 7, 20, 19, 24, 54, 251, DateTimeKind.Local).AddTicks(712), @"Doloribus est distinctio perspiciatis pariatur.
                Quaerat culpa nihil sit officia.
                Minima quaerat corporis cupiditate quo consectetur.
                Quae eaque aut mollitia velit neque nulla non totam nisi.
                Ea et alias quia maiores et.", new DateTime(2020, 6, 30, 0, 53, 30, 756, DateTimeKind.Local).AddTicks(1683), "non", 44, 1, 3 },
                    { 46, new DateTime(2017, 10, 1, 21, 50, 29, 594, DateTimeKind.Local).AddTicks(9019), @"Labore earum dolor recusandae voluptate sequi enim est mollitia.
                Optio accusamus vitae nemo.
                Praesentium sed harum consequuntur vel numquam quas autem voluptatem accusantium.
                Eveniet aspernatur atque laudantium magni quos.
                Ipsum quos aliquam ea qui.", new DateTime(2020, 1, 7, 8, 30, 38, 331, DateTimeKind.Local).AddTicks(4702), "sed", 46, 5, 1 },
                    { 45, new DateTime(2018, 11, 19, 13, 59, 17, 866, DateTimeKind.Local).AddTicks(7683), @"Distinctio non quisquam porro.
                Voluptas odit qui et natus.", new DateTime(2020, 4, 15, 11, 17, 10, 740, DateTimeKind.Local).AddTicks(447), "tempora", 45, 1, 3 },
                    { 50, new DateTime(2019, 2, 19, 3, 39, 21, 636, DateTimeKind.Local).AddTicks(2897), @"Illo aliquam ipsa aliquid corporis consequuntur officiis et.
                Facilis voluptas blanditiis dolores culpa laborum.", new DateTime(2020, 1, 27, 6, 12, 36, 952, DateTimeKind.Local).AddTicks(196), "qui", 50, 1, 3 },
                    { 1, new DateTime(2018, 2, 12, 9, 2, 8, 72, DateTimeKind.Local).AddTicks(7791), @"Autem fuga ut sint nesciunt ad aliquid.
                Sed repudiandae cumque natus dicta rem ipsam voluptas ea.", new DateTime(2020, 7, 5, 14, 32, 44, 840, DateTimeKind.Local).AddTicks(3917), "laborum", 1, 2, 3 },
                    { 4, new DateTime(2018, 11, 8, 4, 48, 17, 25, DateTimeKind.Local).AddTicks(1028), @"Aut adipisci rerum eum esse consequatur eius sint qui.
                Soluta exercitationem recusandae voluptatibus nihil.
                Consequatur explicabo saepe non illo voluptatibus fugit id.", new DateTime(2019, 12, 13, 11, 5, 13, 658, DateTimeKind.Local).AddTicks(2649), "ea", 4, 2, 1 },
                    { 11, new DateTime(2015, 9, 24, 17, 58, 36, 626, DateTimeKind.Local).AddTicks(3213), @"Sit vel qui et minus blanditiis quia.
                Id rerum omnis.
                Voluptates aspernatur inventore reprehenderit.
                Impedit soluta voluptatum fuga sed nam veniam.", new DateTime(2022, 5, 26, 23, 16, 41, 120, DateTimeKind.Local).AddTicks(1613), "qui", 11, 2, 4 },
                    { 23, new DateTime(2018, 10, 4, 22, 35, 40, 86, DateTimeKind.Local).AddTicks(5016), @"Dolores vero aspernatur repudiandae earum voluptatem.
                Nisi incidunt sit inventore maiores sunt officia.
                Eos eum rerum voluptatum.
                Amet autem perferendis.
                Nihil ab repellat soluta sapiente delectus ipsum voluptas exercitationem quia.
                Similique alias a totam quis cumque et fugit.", new DateTime(2020, 4, 4, 17, 47, 8, 703, DateTimeKind.Local).AddTicks(4265), "modi", 23, 2, 4 },
                    { 26, new DateTime(2016, 8, 1, 19, 28, 50, 210, DateTimeKind.Local).AddTicks(8617), @"Voluptatem explicabo aut quas porro rerum rem eos dolores inventore.
                Recusandae corporis ea quo aut est nihil.", new DateTime(2021, 1, 26, 1, 31, 1, 223, DateTimeKind.Local).AddTicks(1178), "eum", 26, 2, 3 },
                    { 33, new DateTime(2019, 6, 27, 12, 55, 21, 889, DateTimeKind.Local).AddTicks(8984), @"Reiciendis quis nulla magnam porro aperiam et animi possimus.
                Omnis accusantium totam.
                Magnam quo qui soluta eius sequi molestiae illum.
                Numquam quo animi et nulla praesentium officiis dolor aut.
                Cumque ab minus.", new DateTime(2020, 5, 5, 12, 36, 20, 219, DateTimeKind.Local).AddTicks(451), "et", 33, 2, 1 },
                    { 2, new DateTime(2019, 3, 6, 4, 6, 36, 283, DateTimeKind.Local).AddTicks(4980), @"Dolor rem dolorum alias laudantium.
                Qui esse consequatur porro porro laudantium.
                Maxime reiciendis ut deserunt.
                Facilis alias aut dolores.
                Illum eius autem consequuntur sed cum voluptatibus.
                Sed qui et autem et.", new DateTime(2019, 10, 5, 23, 16, 22, 725, DateTimeKind.Local).AddTicks(5221), "minus", 2, 3, 2 },
                    { 6, new DateTime(2017, 10, 2, 18, 17, 17, 436, DateTimeKind.Local).AddTicks(5419), @"Facilis assumenda quo natus.
                Tempora quibusdam aliquid consectetur aperiam aperiam aperiam a ut et.
                Dolorem error tempora veritatis quo officia sunt quas repudiandae doloremque.", new DateTime(2020, 7, 27, 15, 40, 25, 955, DateTimeKind.Local).AddTicks(3805), "excepturi", 6, 3, 4 },
                    { 7, new DateTime(2018, 12, 9, 12, 39, 38, 221, DateTimeKind.Local).AddTicks(6042), @"Dignissimos nam aliquam eligendi et.
                Corrupti a excepturi.
                Ab sed totam.", new DateTime(2021, 4, 2, 23, 29, 51, 943, DateTimeKind.Local).AddTicks(5461), "iure", 7, 3, 2 },
                    { 47, new DateTime(2016, 8, 23, 5, 7, 11, 843, DateTimeKind.Local).AddTicks(3997), @"Deleniti sint reiciendis libero molestiae maxime debitis itaque culpa.
                Similique occaecati voluptates delectus quaerat ipsum sed magni.
                Quia perspiciatis voluptates.
                Expedita aut consequatur excepturi natus consequatur est quaerat rem.", new DateTime(2019, 11, 23, 15, 32, 31, 98, DateTimeKind.Local).AddTicks(3307), "vel", 47, 1, 4 },
                    { 49, new DateTime(2019, 6, 21, 12, 11, 9, 128, DateTimeKind.Local).AddTicks(7876), @"Eligendi deserunt nesciunt ipsam inventore ut voluptatem.
                Ea magnam explicabo veniam ratione rerum sunt inventore reprehenderit aut.
                Magnam nihil voluptatem nostrum qui nam veniam qui dolores.", new DateTime(2020, 7, 5, 19, 36, 21, 672, DateTimeKind.Local).AddTicks(7863), "dolor", 49, 5, 1 }
                });
        }
    }
}
