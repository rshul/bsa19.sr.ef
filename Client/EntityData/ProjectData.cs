﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.EntityData
{
    public class ProjectData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public UserData Author { get; set; }
        public TeamData Team { get; set; }
        public List<TaskData> TasksDatas { get; set; }

    }
}
