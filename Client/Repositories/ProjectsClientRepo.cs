﻿using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositories
{
    public class ProjectsClientRepo : IRepo<ProjectDTO>
    {

        private string path = "Projects";

        public ProjectsClientRepo()
        {
            HttpService.SetBaseUrl (@"https://localhost:5001/api/");
        }
        public async Task Delete(int id)
        {
           await HttpService.DeleteObjectAsync(id, path);
        }

        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            return await HttpService.GetObjectsAsync<ProjectDTO>(path);
        }

        public async Task<ProjectDTO> GetById(int id)
        {
            return await HttpService.GetObjecttAsync<ProjectDTO>(id , path);
        }

        public async Task Insert(ProjectDTO entity)
        {
            await HttpService.CreateProductAsync<ProjectDTO>(entity, path);
        }

        public async Task Update(int id, ProjectDTO entity)
        {
           await  HttpService.UpdateObjectAsync<ProjectDTO>(id, entity, path);
        }
    }
}
