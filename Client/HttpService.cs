﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;

namespace Client
{
    public class HttpService
    {
        private static readonly HttpClient _client = new HttpClient();

        public static void SetBaseUrl(string url)
        {
            _client.BaseAddress = new Uri(url);
        }

        public static async Task<IEnumerable<T>> GetObjectsAsync<T>(string path)
        {
            IEnumerable<T> objects = null;
            HttpResponseMessage response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                objects = await response.Content.ReadAsAsync<IEnumerable<T>>();
            }
            return objects;
        }

       public static async Task<T> UpdateObjectAsync<T>(int id, T entity, string path) where T : class
        {
            HttpResponseMessage response = await _client.PutAsJsonAsync(
                $"{path}/{id}", entity);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            entity = await response.Content.ReadAsAsync<T>();
            return entity;
        }

       public static async Task<HttpStatusCode> DeleteObjectAsync(int id, string path)
        {
            HttpResponseMessage response = await _client.DeleteAsync(
                $"{path}/{id}");
            return response.StatusCode;
        }

        public static async Task<Uri> CreateProductAsync<T>(T entity, string path)
        {
            HttpResponseMessage response = await _client.PostAsJsonAsync(
                path, entity);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }

       public static async Task<T> GetObjecttAsync<T>(int id, string path) where T : class
        {
            T entity = null;
            HttpResponseMessage response = await _client.GetAsync($"{path}/{id}");
            if (response.IsSuccessStatusCode)
            {
                entity = await response.Content.ReadAsAsync<T>();
            }
            return entity;
        }
    }
}
