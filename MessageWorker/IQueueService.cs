﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageWorker
{
    public interface IQueueService
    {
        void PushMessage(string message);
    }
}
